package com.jspiders.webApplication.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jspiders.webApplication.constants.AppConstants;
import com.jspiders.webApplication.dto.Login;
import com.jspiders.webApplication.entity.Register;
import com.jspiders.webApplication.repository.AuthorizationRepository;
import com.jspiders.webApplication.util.AppResponse;

@Service
public class AuthorizationService {
	
	@Autowired
	private AuthorizationRepository authorizationRepository;
	
	public AuthorizationService() {
		System.out.println(this.getClass().getSimpleName()+" class object created");
	}
	
	public void saveRegisterDetails(Register register)
	{
		System.out.println("executing saveRegisterDetails() of AuthorizationService class"+register);
		
		authorizationRepository.saveRegisterDetails(register);
	}
	
	public Register loginDetails(Login login)
	{
		return authorizationRepository.getByEmailAndPassword(login.getEmail(), login.getPassword());
	}
	
	public AppResponse findAll()
	{
		AppResponse appResponse=null;
		
		try {
			List<Register> registerList = authorizationRepository.findAll();
			appResponse= new AppResponse(AppConstants.SUCCESS, "200", registerList, null);
		} catch (Exception e) {
			appResponse= new AppResponse(AppConstants.FAILURE, "500", null, e.getMessage());
		}
		
		return appResponse;
		
	}
	
	public Register findByContactNumber(String contactNumber)
	{
		return authorizationRepository.findByContactNumber(contactNumber);
	}
	
	public AppResponse findByName(String name)
	{
		AppResponse appResponse=null;
		
		try {
			Register register = authorizationRepository.findByName(name);
			appResponse= new AppResponse(AppConstants.SUCCESS, "200", register, null);
		} catch (Exception e) {
			appResponse= new AppResponse(AppConstants.FAILURE, "500", null, e.getMessage());
		}
		
		return appResponse;
		
	}
	
	

}
