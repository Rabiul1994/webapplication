package com.jspiders.webApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jspiders.webApplication.constants.AppConstants;
import com.jspiders.webApplication.dto.Login;
import com.jspiders.webApplication.entity.Register;
import com.jspiders.webApplication.service.AuthorizationService;

@RequestMapping(value=AppConstants.FORWARD_SLASH)
@Controller
public class AuthorizationController {
	
	@Autowired
	private AuthorizationService authorizationService;
	
	public AuthorizationController() {
		System.out.println(this.getClass().getSimpleName()+" class object created");
	}
	
	
//	@RequestMapping(value= AppConstants.SAVE_REGISTER_DETAILS)
	@PostMapping(value= AppConstants.SAVE_REGISTER_DETAILS)
	public ModelAndView saveRegisterDetails(Register register)
	{
		//logic
		
		System.out.println("executing saveRegisterDetails() of AuthorizationController class"+register);
		
		authorizationService.saveRegisterDetails(register);
		return new ModelAndView("login.jsp");
	}
	
//	@RequestMapping(value=AppConstants.LOGIN_DETAILS, method=RequestMethod.POST)
	@PostMapping(value=AppConstants.LOGIN_DETAILS)
	public ModelAndView loginDetails(Login login)
	{
		Register register = authorizationService.loginDetails(login);
		
		if(register!=null) {
				return new ModelAndView("home.jsp");
		}
		else return new ModelAndView("login.jsp");
	}

}
