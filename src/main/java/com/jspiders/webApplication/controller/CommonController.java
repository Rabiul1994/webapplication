package com.jspiders.webApplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jspiders.webApplication.constants.AppConstants;
import com.jspiders.webApplication.entity.Register;
import com.jspiders.webApplication.service.AuthorizationService;
import com.jspiders.webApplication.util.AppResponse;

//@Controller
//@ResponseBody
@RestController
@RequestMapping(value=AppConstants.FORWARD_SLASH)
public class CommonController 
{
	@Autowired
	private AuthorizationService authorizationService;
	
	@GetMapping(value=AppConstants.FIND_ALL)
	public @ResponseBody AppResponse findAll()
	{
		return authorizationService.findAll();
	}
	
	@PostMapping(value=AppConstants.SAVE)
	public void saveDetails(@RequestBody Register register)
	{
		authorizationService.saveRegisterDetails(register);
	}
	
	@GetMapping(value= AppConstants.FIND_BY_CONTACT)
	public Register findByContactNumber(@PathVariable("mobile") String contactNumber)
	{
		return authorizationService.findByContactNumber(contactNumber);
	}
	
	@GetMapping(value=AppConstants.FIND_BY_NAME)
	public AppResponse findByName(@RequestHeader("name") String name)
	{
		return authorizationService.findByName(name);
	}
}
