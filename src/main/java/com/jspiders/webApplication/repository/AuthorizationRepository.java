package com.jspiders.webApplication.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jspiders.webApplication.entity.Register;

@Repository
public class AuthorizationRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public void saveRegisterDetails(Register register)
	{
		System.out.println("executing saveRegisterDetails() of AuthorizationRepository class"+register);
		
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(register);
		transaction.commit();
		
		System.out.println("Saved into database Successfully");
	}
	
	public Register getByEmailAndPassword(String email, String password)
	{
		Session session = sessionFactory.openSession();
		String hql="from Register where email=:em and password=:pwd";
		Query query = session.createQuery(hql);
		query.setParameter("em", email);
		query.setParameter("pwd", password);
		return (Register) query.uniqueResult();
	}
	
	public List<Register> findAll()
	{
		String hql="from Register";
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(hql);
		return query.list();
	}
	
	public Register findByContactNumber(String contactNumber)
	{
		Session session = sessionFactory.openSession();
		String qry="from Register where contactNumber=:cn";
		Query query = session.createQuery(qry);
		query.setParameter("cn", contactNumber);
		return (Register) query.uniqueResult();
	}
	
	public Register findByName(String name)
	{
		Session session = sessionFactory.openSession();
		String qry="from Register where name=:n";
		Query query = session.createQuery(qry);
		query.setParameter("n", name);
		return (Register) query.uniqueResult();
	}

}
