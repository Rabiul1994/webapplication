package com.jspiders.webApplication.constants;

public interface AppConstants {
	
	public static String USER_REGISTRATION_INFO="user_register";
	public static String SAVE_REGISTER_DETAILS="saveRegisterDetails";
	public static String FORWARD_SLASH="/";
	public static String LOGIN_DETAILS="loginDetails";
	public static String FIND_ALL="findAll";
	public static String SAVE="saveDetails";
	public static String FIND_BY_CONTACT="/findByContactNumber/{mobile}";
	public static String SUCCESS="Success";
	public static String FAILURE="Failure";
	public static String FIND_BY_NAME="/findByName";
	
	

}
