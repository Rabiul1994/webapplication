<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<body>
    <form action="saveRegisterDetails", method="post">
  <div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control" name="name">
  </div>
  <div class="mb-3">
    <label for="name" class="form-label">Email</label>
    <input type="email" class="form-control" name="email">
  </div>
  <div class="mb-3">
    <label for="name" class="form-label">Contact Number</label>
    <input type="tel" class="form-control" name="contactNumber">
  </div>
  <div class="mb-3">
    <label for="name" class="form-label">Country</label>
    <input type="text" class="form-control" name="country">
  </div>
  <div class="mb-3">
    <label for="name" class="form-label">Password</label>
    <input type="password" class="form-control" name="password">
  </div>
 
  
  <button type="submit" class="btn btn-primary">Register</button>
</form>
</body>
</html>